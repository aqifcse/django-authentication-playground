from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import User


# Create your views here.
def home(request):

    if 'user' in request.session:
        current_user = request.session['user']
        param = {'current_user': current_user}
        return render(request, 'users/base.html', param)
    else:
        return redirect('sessionApp:login')
    return render(request, 'sessionApp/login.html')



def signup(request):
    if request.method == 'POST':
        uname = request.POST.get('uname')
        pwd = request.POST.get('pwd')
        # print(uname, pwd)
        if User.objects.filter(username=uname).count()>0:
            return HttpResponse('Username already exists.')
        else:
            user = User(username=uname, password=pwd)
            user.save()
            return redirect('sessionApp:login')
    else:
        return render(request, 'users/signup.html')



def login(request):
    # If we submitted the form...
    if request.method == 'POST':
        uname = request.POST.get('uname')
        pwd = request.POST.get('pwd')

        check_user = User.objects.filter(username=uname, password=pwd)
        # Check that the test cookie worked (we set it below):
        if check_user:
            request.session['user'] = uname
            return redirect('sessionApp:home')
        else:
            return HttpResponse('Please enter valid Username or Password.')

    return render(request, 'users/login.html')

# def login(request):

#     # If we submitted the form...
#     if request.method == 'POST':

#         # Check that the test cookie worked (we set it below):
#         if request.session.test_cookie_worked():
#             print('COOKIE WORKED !!!!!!!!!!!!!')

#             # The test cookie worked, so delete it.
#             request.session.delete_test_cookie()
#             print('COOKIE DELETED !!!!!!!!!!!!!')

#             # In practice, we'd need some logic to check username/password
#             # here, but since this is an example...
#             return HttpResponse("You're logged in.")

#         # The test cookie failed, so display an error message. If this
#         # were a real site, we'd want to display a friendlier message.
#         else:
#             print('COOKIE DIDNT WORKED :( !!!!!!!!!!!!!')
#             return HttpResponse("Please enable cookies and try again.")

#     # If we didn't post, send the test cookie along with the login form.
#     request.session.set_test_cookie()
#     return render(request,'users/login.html')


def logout(request):
    try:
        del request.session['user']
    except:
        return redirect('sessionApp:login')
    return redirect('sessionApp:login')


# def test_cookie(request):   
#     if not request.COOKIES.get('color'):
#         response = HttpResponse("Cookie Set")
#         response.set_cookie('color', 'blue', 5)
#         return response
#     else:
#         return HttpResponse("Your favorite color is {0}".format(request.COOKIES['color']))

# def delete_test_cookie(request):   
#     if request.COOKIES.get('color'):
#        response = HttpResponse("Cookies Cleared")
#        response.delete_cookie("color")
#     else:
#         response = HttpResponse("We are not tracking you.")
#     return response

# def track_user(request):
#     if not request.COOKIES.get('visits'):
#         response = HttpResponse("This is your first visit to the site. "
#                                 "From now on I will track your vistis to this site.")
#         response.set_cookie('visits', '1', 5)
#     else:
#         visits = int(request.COOKIES.get('visits')) + 1
#         response = HttpResponse("This is your {0} visit".format(visits))
#         response.set_cookie('visits', str(visits),  5)
#     return response

# def track_user(request):
#     response = render(request, 'tracks/track_user.html') # store the response in response variable
#     if not request.COOKIES.get('visits'):        
#         response.set_cookie('visits', '1', 5)
#     else:
#         visits = int(request.COOKIES.get('visits', '1')) + 1
#         response.set_cookie('visits', str(visits),  5)
#     return response


# def stop_tracking(request):
#     if request.COOKIES.get('visits'):
#        response = HttpResponse("Cookies Cleared")
#        response.delete_cookie("visits")
#     else:
#         response = HttpResponse("We are not tracking you.")
#     return response

#----------------------Session CRUD-----------------------------------------

# def create_session(request):
#     request.session['name'] = 'aku_man2'
#     request.session['password'] = 'aku@man2'
#     return HttpResponse("<h1>dataflair<br> the session is set</h1>")

# def access_session(request):
#     response = "<h1>Welcome to Sessions of dataflair</h1><br>"
#     if request.session.get('name'):
#         response += "Name : {0} <br>".format(request.session.get('name'))
#     if request.session.get('password'):
#         response += "Password : {0} <br>".format(request.session.get('password'))
#         return HttpResponse(response)
#     else:
#         return redirect('create/')

# def delete_session(request):
#     try:
#         del request.session['name']
#         del request.session['password']
#     except KeyError: pass
#     return HttpResponse("<h1>dataflair<br>Session Data cleared</h1>")

#----------------------------------- JWT Authentication-------------------
# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
  
  
# class HelloView(APIView):
#     permission_classes = (IsAuthenticated, )
  
#     def get(self, request):
#         content = {'message': 'Hello, User'}
#         return Response(content)


