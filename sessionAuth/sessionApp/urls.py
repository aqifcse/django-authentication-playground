from django.urls import path
from . import views

app_name = 'sessionApp'

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('signup/', views.signup, name='signup'),

    # cookie tracking urls
    # path('test-cookie/', views.test_cookie, name='test_cookie'),
    # path('delete-test-cookie/', views.delete_test_cookie, name='delete_test_cookie'),
    # path('track_user/', views.track_user, name='track_user'),
    # path('stop-tracking/', views.stop_tracking, name='stop_tracking'),
    #-------------------Session CRUD-------------------------------------
    # path('create/', views.create_session, name='create'),
    # path('access/', views.access_session, name='access'),
    # path('delete/', views.delete_session, name='delete'),

    #------------------JWT Auth------------------------------
    # path('hello/', views.HelloView.as_view(), name ='hello'),

]