django
djangorestframework==1.7.1
PyJWT==3.12.1
djangorestframework_simplejwt==4.4.0

social-auth-app-django==4.0.0
social-auth-app-django-mongoengine==1.0.0

